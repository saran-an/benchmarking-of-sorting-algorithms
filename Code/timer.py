import timeit
import copy
import numpy as np


def time_function(sort_function, test_data):
    clock = timeit.Timer(stmt='sort_func(copy(data))',
                         globals={'sort_func': sort_function,
                                  'data': test_data,
                                  'copy': copy.copy})
    n_ar, t_ar = clock.autorange()

    data = np.array(clock.repeat(repeat=7, number=n_ar)) / n_ar

    return np.min(data)
