import pandas as pd
from Code.generate_data import create_random_list
from Code.timer import time_function
from Code.algorithms import merge_sort
from Code.algorithms import quick_sort
from Code.algorithms import insertion_sort

data_size_1 = [20, 40, 60, 80, 100, 120, 140]
data_size_2 = [10, 20, 30, 40]


def benchmark(sort_algorithm, test_size, save_name):
    random_data = [[], []]

    for data_size in test_size:
        random_data[0].append(data_size)

        time_random = time_function(sort_algorithm, create_random_list(data_size))
        random_data[1].append(time_random)

    final_data = [random_data]
    data_frame = pd.DataFrame(final_data)
    save_directory = 'result/'
    data_frame.to_pickle(save_directory+save_name+'.pkl')
    return final_data


insertion_benchmark_merge = benchmark(insertion_sort, data_size_1, 'insertion_sort_merge_interval')
merge_benchmark = benchmark(merge_sort, data_size_1, 'merge_sort_interval')

insertion_benchmark_quick = benchmark(insertion_sort, data_size_2, 'insertion_sort_quick_interval')
quick_benchmark = benchmark(quick_sort, data_size_2, 'quick_sort_interval')
