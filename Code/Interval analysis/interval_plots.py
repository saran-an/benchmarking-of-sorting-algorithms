import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

insertion_merge = pd.read_pickle("result/insertion_sort.pkl", compression=None)
merge_sort = pd.read_pickle("result/merge_sort.pkl", compression=None)
insertion_quick = pd.read_pickle("result/insertion_sort_quick_interval.pkl", compression=None)
quick_sort = pd.read_pickle("result/quick_sort_interval.pkl", compression=None)


# Data size for insertion and merge
data_size_1 = [20, 40, 60, 80, 100, 120, 140]

# Data size for insertion and quick
data_size_2 = [10, 20, 30, 40]


def make_directory(size, algorithm_name):
    directory = {'Size': size, 'create_random_list': algorithm_name[1][0]}
    return directory


data_insertion_merge = make_directory(data_size_1, insertion_merge)
data_merge = make_directory(data_size_1, merge_sort)

data_insertion_quick = make_directory(data_size_2, insertion_quick)
data_quick = make_directory(data_size_2, quick_sort)


df_insertion_merge = pd.DataFrame(data_insertion_merge)
df_merge = pd.DataFrame(data_merge)

df_insertion_quick = pd.DataFrame(data_insertion_quick)
df_quick = pd.DataFrame(data_quick)


def plot(data1, data2, label_1, label_2, save):
    plt.rcParams['axes.titlesize'] = 9
    plt.rcParams['axes.labelsize'] = 9
    plt.rcParams['xtick.labelsize'] = 8
    plt.rcParams['ytick.labelsize'] = 8
    plt.rcParams['legend.fontsize'] = 8
    plt.rcParams['text.usetex'] = True
    fig = plt.figure(figsize=(84/25.4, 55/25.4))
    fig.add_subplot(1, 1, 1)

    sns.lineplot(x='Size', y='create_random_list', data=data1, label=label_1, marker='*')
    sns.lineplot(x='Size', y='create_random_list', data=data2, label=label_2, marker='*')
    plt.ylabel('Time (s)')
    plt.xlabel('Size of test data')
    plt.title(' Analysis of interval with random list')
    plt.show()
    directory = 'plots/'
    fig.savefig(directory + save, bbox_inches='tight')


plot(df_insertion_merge, df_merge, "Insertion sort", "Merge sort", 'insertion_merge.pdf')
plot(df_insertion_quick, df_quick, "Insertion sort", "Quick sort", 'insertion_quick.pdf')
