import pandas as pd
from generate_data import create_ascending_list, create_descending_list
from generate_data import creat_random_caract_list, create_random_list, creat_random_float_list
from timer import time_function
from algorithms import insertion_sort, bubble_sort, merge_sort, quick_sort
from algorithms import merge_combined, quicksort_combined, python_sort, numpy_sort

# Data size for insertion, bubble, quick and quick combined
data_size_1 = [10, 100, 1000, 10000]

# Data size for merge, merge combined, python and NumPy
data_size_2 = [10, 100, 1000, 10000, 100000]


def benchmark(sort_algorithm, test_size, save_name):
    sorted_data = [[], []]
    reversed_data = [[], []]
    letter_data = [[], []]
    random_data = [[], []]
    random_float_data = [[], []]

    for data_size in test_size:
        sorted_data[0].append(data_size)
        reversed_data[0].append(data_size)
        letter_data[0].append(data_size)
        random_data[0].append(data_size)
        random_float_data[0].append(data_size)

        time_sorted = time_function(sort_algorithm, create_ascending_list(data_size))
        reversed_data[1].append(time_sorted)

        time_reserved = time_function(sort_algorithm, create_descending_list(data_size))
        sorted_data[1].append(time_reserved)

        time_letter = time_function(sort_algorithm, creat_random_caract_list(data_size))
        letter_data[1].append(time_letter)

        time_random = time_function(sort_algorithm, create_random_list(data_size))
        random_data[1].append(time_random)

        time_random_float = time_function(sort_algorithm, creat_random_float_list(data_size))
        random_float_data[1].append(time_random_float)

    final_data = [sorted_data, reversed_data, letter_data, random_data, random_float_data]
    data_frame = pd.DataFrame(final_data)
    save_directory = 'result/'
    data_frame.to_pickle(save_directory+save_name+'.pkl')
    return final_data


insertion_benchmark = benchmark(insertion_sort, data_size_1, 'insertion_sort')
bubble_benchmark = benchmark(bubble_sort, data_size_1, 'bubble_sort')
merge_benchmark = benchmark(merge_sort, data_size_2, 'merge_sort')
merge_combined_benchmark = benchmark(merge_combined, data_size_2, 'merge_combined_sort')
python_benchmark = benchmark(python_sort, data_size_2, 'python_sort')
numpy_benchmark = benchmark(numpy_sort, data_size_2, 'numpy_sort')


# Benchmark of quick sort
test_size_quick_1 = [10, 100, 1000]
test_size_quick_2 = [10, 100, 1000, 10000]


def benchmark_quick(sort_algorithm, data_type, algorithm_name):
    data_set_1 = [[], []]
    data_set_2 = [[], []]
    data_set_3 = [[], []]

    if data_type in ['ascending ans decending list']:
        for data_size in test_size_quick_1:
            data_set_1[0].append(data_size)
            data_set_2[0].append(data_size)
            data_set_1[1].append(time_function(sort_algorithm, create_ascending_list(data_size)))
            data_set_2[1].append(time_function(sort_algorithm, create_descending_list(data_size)))

        all_data = [data_set_1, data_set_2]
    else:
        for data_size in test_size_quick_2:
            data_set_1[0].append(data_size)
            data_set_2[0].append(data_size)
            data_set_3[0].append(data_size)
            data_set_1[1].append(time_function(sort_algorithm, create_random_list(data_size)))
            data_set_2[1].append(time_function(sort_algorithm, creat_random_float_list(data_size)))
            data_set_3[1].append(time_function(sort_algorithm, creat_random_caract_list(data_size)))

        all_data = [data_set_1, data_set_2, data_set_3]

    data_frame = pd.DataFrame(all_data)

    directory = 'result/'
    data_frame.to_pickle(directory + algorithm_name + '.pkl')

    return all_data


quick_benchmark_small = benchmark_quick(quick_sort, 'ascending ans decending list', 'quick_sort_small')
quick_benchmark_big = benchmark_quick(quick_sort, 'random', 'quick_sort_big')

quick_combined_small = benchmark_quick(quicksort_combined, 'ascending ans decending list', 'quick_combined_small')
quick_combined_big = benchmark_quick(quicksort_combined, 'random', 'quick_combined_big')
