"""
name: Majorann Thevarajah
email: majorann.thevarajah@nmbu.no
"""
import math
import numpy as np


# Insertion sort
def insertion_sort(A):
    for j in range(1, len(A)):
        key = A[j]
        i = j - 1
        while i >= 0 and A[i] > key:
            A[i + 1] = A[i]
            i = i - 1
        A[i + 1] = key

    return A


# Bubble sort
def bubble_sort(A):
    for i in range(len(A)):
        for j in range(len(A)-1, i, -1):
            if A[j] < A[j - 1]:
                A[j], A[j - 1] = A[j - 1], A[j]
    return A


# Merge sort
def merge(a, p, q, r):
    n_1 = q - p + 1
    n_2 = r - q

    left = [0] * n_1
    right = [0] * n_2

    for m in list(range(n_1)):
        left[m] = a[p + m]

    for t in list(range(n_2)):
        right[t] = a[q + t + 1]

    i = 0
    j = 0

    for k in list(range(p, r + 1)):
        if i >= n_1:
            a[k] = right[j]
            j = j + 1
        elif j >= n_2:
            a[k] = left[i]
            i = i + 1
        elif left[i] <= right[j]:
            a[k] = left[i]
            i = i + 1
        else:
            a[k] = right[j]
            j = j + 1


def merge__sort(a, p, r):
    if p < r:
        q = math.floor((p + r) / 2)
        merge__sort(a, p, q)
        merge__sort(a, q + 1, r)
        merge(a, p, q, r)


def merge_sort(a):
    s = (len(a) - 1)
    merge__sort(a, 0, s)
    return a


# Quicksort
def quicksort(array, low, high):
    if low < high:
        pivot = partition(array, low, high)
        quicksort(array, low, pivot-1)
        quicksort(array, pivot+1, high)


def partition(array, low, high):
    pivot = array[high]
    i = (low - 1)

    for j in range(low, high):
        if array[j] <= pivot:
            i = i + 1
            array[i], array[j] = array[j], array[i]
    array[i + 1], array[high] = array[high], array[i + 1]
    return i + 1


def quick_sort(data):

    return quicksort(data, 0, len(data) - 1)


# Merge combined with insertion
def merge_combined(A):
    if len(A) <= 72:
        return insertion_sort(A)
    else:
        return merge_sort(A)


# Quick combined with insertion
def quicksort_combined(A):
    if len(A) <= 23:
        return insertion_sort(A)
    else:
        return quick_sort(A)


# Numpy sort
def numpy_sort(A):
    return np.sort(A)


# Python sort
def python_sort(A):
    return sorted(A)
