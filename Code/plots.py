import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

insertion_sort = pd.read_pickle("result/insertion_sort.pkl", compression=None)
bubble_sort = pd.read_pickle("result/bubble_sort.pkl", compression=None)
merge_sort = pd.read_pickle("result/merge_sort.pkl", compression=None)
quick_sort_small = pd.read_pickle("result/quick_sort_small.pkl", compression=None)
quick_sort_big = pd.read_pickle("result/quick_sort_big.pkl", compression=None)
mergesort_combined_sort = pd.read_pickle("result/merge_combined_sort.pkl", compression=None)
quicksort_combined_small = pd.read_pickle("result/quick_combined_small.pkl", compression=None)
quicksort_combined_big = pd.read_pickle("result/quick_combined_big.pkl", compression=None)
python_sort = pd.read_pickle("result/python_sort.pkl", compression=None)
numpy_sort = pd.read_pickle("result/numpy_sort.pkl", compression=None)


# Data size for insertion, bubble, quick and quick combined
data_size_1 = [10, 100, 1000, 10000]

# Data size for merge, merge combined, python and NumPy
data_size_2 = [10, 100, 1000, 10000, 100000]


def make_directory(size, algorithm_name):
    directory = {'Size': size, 'create_ascending_list': algorithm_name[1][0],
                 'create_descending_list': algorithm_name[1][1],
                 'creat_random_caract_list': algorithm_name[1][2],
                 'create_random_list': algorithm_name[1][3],
                 'creat_random_float_list': algorithm_name[1][4]}
    return directory


data_insertion = make_directory(data_size_1, insertion_sort)
data_bubble = make_directory(data_size_1, bubble_sort)
data_merge = make_directory(data_size_2, merge_sort)
data_merge_combined = make_directory(data_size_2, mergesort_combined_sort)
data_python = make_directory(data_size_2, python_sort)
data_numpy = make_directory(data_size_2, numpy_sort)


def make_directory_quick(data_type, size, algorithm_name):
    if data_type in ['ascending ans decending list']:
        directory = {'Size': size, 'create_ascending_list': algorithm_name[1][0],
                     'create_descending_list': algorithm_name[1][1]}
    else:
        directory = {'Size': size,'creat_random_caract_list': algorithm_name[1][0],
                     'create_random_list': algorithm_name[1][1],
                     'creat_random_float_list': algorithm_name[1][2]}
    return directory


size_quick_1 = [10, 100, 1000]
size_quick_2 = [10, 100, 1000, 10000]

data_quick_small = make_directory_quick('ascending ans decending list', size_quick_1, quick_sort_small)
data_quick_big = make_directory_quick('random', size_quick_2, quick_sort_big)
data_quick_combined_small = make_directory_quick('ascending ans decending list', size_quick_1, quicksort_combined_small)
data_quick_combined_big = make_directory_quick('random', size_quick_2, quicksort_combined_big)

df_insertion = pd.DataFrame(data_insertion)
df_bubble = pd.DataFrame(data_bubble)
df_merge = pd.DataFrame(data_merge)
df_quick_small = pd.DataFrame(data_quick_small)
df_quick_big = pd.DataFrame(data_quick_big)
df_merge_combined = pd.DataFrame(data_merge_combined)
df_quick_combined_small = pd.DataFrame(data_quick_combined_small)
df_quick_combined_big = pd.DataFrame(data_quick_combined_big)
df_python = pd.DataFrame(data_python)
df_numpy = pd.DataFrame(data_numpy)
print(df_numpy)


def plot(data_type, quick, quick_combined, title_name, save):
    plt.rcParams['axes.titlesize'] = 9
    plt.rcParams['axes.labelsize'] = 9
    plt.rcParams['xtick.labelsize'] = 8
    plt.rcParams['ytick.labelsize'] = 8
    plt.rcParams['legend.fontsize'] = 8
    plt.rcParams['text.usetex'] = True
    fig = plt.figure(figsize=(84/25.4, 55/25.4))
    ax = fig.add_subplot(1, 1, 1)

    sns.lineplot(x='Size', y=data_type, data=df_insertion, label="Insertion sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=df_bubble, label="Bubble sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=df_merge, label="Merge sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=quick, label="Quick sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=df_merge_combined, label="Merge combined sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=quick_combined, label="Quick combined sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=df_python, label="Python sort", marker='*')
    sns.lineplot(x='Size', y=data_type, data=df_numpy, label="Numpy sort", marker='*')

    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.ylabel('Time (s)')
    plt.xlabel('Size of test data')
    plt.title(title_name)

    ax.legend()
    ax.get_legend().remove()
    plt.show()
    directory = 'plots/'
    fig.savefig(directory + save, bbox_inches='tight')

    # fig2 = plt.figure(figsize=(84/25.4, 55/25.4))
    # ax2 = fig2.add_subplot(1, 1, 1)
    # h, l = ax.get_legend_handles_labels()
    # ax2.clear()
    # ax2.legend(h, l, loc='center')
    # plt.axis('off')
    # plt.show()

    # fig2.savefig('plots/labels.pdf')


plot('create_ascending_list', df_quick_small, df_quick_combined_small, 'Analysis of sorted list', 'ascending_plot.pdf')
plot('create_descending_list', df_quick_small, df_quick_combined_small, 'Analysis of descending list', 'descending_plot.pdf')
plot('creat_random_caract_list', df_quick_big, df_quick_combined_big, 'Analysis of random list with letters', 'random_letter_list_plot.pdf')
plot('create_random_list', df_quick_big, df_quick_combined_big, 'Analysis of random list', 'random_list_plot.pdf')
plot('creat_random_float_list', df_quick_big, df_quick_combined_big, 'Analysis of random float list', 'random_float_list_plot.pdf')
