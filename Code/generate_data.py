import random


def create_random_list(n_elements):
    """
    Create a list where it returns a list in random order between the interval og -n_elements and n_elements, where n
    is the length.
    """
    list_data = [random.randint(-n_elements, n_elements) for _ in range(n_elements)]
    return list_data


def create_ascending_list(n_elements):
    """
    Create a list where it returns a list in ascending order. The values will be from 0 to n_elements, where
    n_elements is the length.
    """
    list_data = [j for j in range(n_elements)]
    return list_data


def creat_random_caract_list(n_elements):
    """
    Returns a list of length n_elements with random characters
    Create a list where it returns random characters with the length of n_elements.
    """
    list_data = [random.choice('abcdefghisjklmnopqrstuvwxyz')
                 for _ in range(n_elements)]

    return list_data


def create_descending_list(n_elements):
    """
    Create a list where it returns a list in descending order. The values will be from n_elements to 0,
    where n_elements is the length.
    """
    list_data = [k for k in range(n_elements - 1, -1, -1)]
    return list_data


def creat_random_float_list(n_elements):
    """
    Create a list where it returns a list with float values in random order between the interval og -n_elements and
    n_elements, where n_elements is the length.
    """
    list_data = [random.uniform(-n_elements, n_elements) for _ in range(n_elements)]
    return list_data
