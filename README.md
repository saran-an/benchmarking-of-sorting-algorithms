# Benchmarking of Sorting Algorithms

###  Description
In this paper we are using accumulated lessons 
to analyze, compare, and perform benchmarks
against eight sorting algorithms implemented in Python. Our motivation is to get an indicator and to use the acquired knowledge to
study sorting algorithms further. The eight algorithms 
are listed below sorted into four categories,
quadratic, sub-quadratic, combined algorithms and the built-in sorting functions.

##### Quadratic algorithms 
* Insertion sort
* Bubble Sort
##### Sub-quadratic algorithms 
* Mergesort
* Quicksort
##### Combined algorithms
* Mergesort combined with Insertion sort
* Quicksort combined with Insertion sort
##### Built-in sorting functions 
* Python ‘sort()‘
* NumPy ‘sort()‘


__This paper will answer following question:__ “How well the different
types of sorting algorithm perform against each other based on the
size and structure of the data?"
   

